# Clase 2: El ciclo celular
## Reglas del juego actualizadas
### Estructura del curso
* El curso tiene 16 clases
* Datos
    * José Antonio López Rodríguez
    * jal.ccs@gmail.com
* Fechas 2024-01-15 al 2024-04-25
* Evaluación
    * Exámenes (ojo)
        * 2024-02-02 (25%)
        * 2024-02-23 (25%)
        * 2024-03-08 (25%)
        * 2024-03-22 (25%)
## Partes del ciclo celular
### Escala grande
* Definición: Estructura autónoma e independiente, delimitada por una membrana que controla el transporte de sustancias del interior al exterior.
* Interfase
* Mitosis
## Muerte celular
## Tipos de muerte celular
* Mitótica
* Apoptosis
* Autofagia
* Senescencia
## El estudio del ciclo celular
* Sincronización de cultivos
* Discriminación a partir de las fases observables
* Hydroxyurea
* Cosecha mitótica
* Duración de las etapas del ciclo
* Indices mitótico y etiquetado
* Relación entre índices y duración de las etapas
  
## Organización de tejidos
* Compartimientos jerárquicos
* Compartimientos flexibles


