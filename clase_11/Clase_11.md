# Clase 11: Daño letal, daño potencialmente letal y daño subletal

## Contenido
* Clasificación del daño celular
* Daño letal
* Daño potencialmente letal
* Daño subletal
* Mecanismos de reparación del daño subletal
* Reparación y calidad de radiación
* Efecto tasa de dosis
 
## Clasificación del daño celular por radiación
* Hay tres categorías
* Daño letal: irreversible
* Daño potencialmente letal: depende de las condiciones post-radiación
* Daño subletal: depende de la dosis
 
## Daño potencialmente letal (PLD)
* El daño es modificado por las condiciones post-radiación
* En la mayoría de las condiciones normales este daño es letal
* El PLD se repara en células inmersas en una solución salina balanceada, en vez de hacerlo en un medio óptimo para el crecimiento
* También se observa el efecto en cultivos en fase estacionaria inhibidos por densidad
* Las células se sensibilizan si son trasladadas a un cultivo de condiciones óptimas inmediatamente luego de la irradiación
 
## Experimento 2
* Se puede ver el mismo efecto en células de tumores con crecimiento inhibido por densidad y en fase estacionaria
* Se irradian tumores en ratones (fibrosarcomas)
* Se toma una muestra inmediatamente y se cultiva en condiciones óptimas
* Las condiciones de un tumor más grande favorecen la reparación PLD
 
## Conclusión
* El PLD se favorece en condiciones sub-óptimas para el crecimiento
* El proceso de mitosis se retrasa
* Hay más tiempo de reparación
 
## Daño Subletal (SLD)
* El daño subletal puede ser reparado si no se acumula más daño subletal
* El daño subletal acumulado, no reparado, produce daño letal
* La supervivencia mejora si una dosis es separada en dos fracciones con un tiempo intermedio
 
## Experimento 3
* Se aplica una dosis de referencia (15,58 Gy) en una población de células de hámster chino
* La fracción de supervivencia es S = 5 e -3
* Sobre otro cultivo se aplica la misma dosis separada en dos fracciones iguales, separadas 30 minutos, 1 hora, 1,5 horas, etc
* Las células permanecen a 24 C entre las dosis para impedir el avance en el ciclo celular
 
## Experimento 4
* Mismas condiciones
* Se permite que el ciclo celular continúe
* El efecto de la reparación del SLD se superpone con los efectos de: 
* Sincronización parcial
* Llegada de la población sincronizada a la etapa G2/M: muy sensible
* Luego se añade el efecto de multiplicación al completar el ciclo
 
## Importante
* Reparación (SLD)
* Reorganización de tejidos (progresión del ciclo celular)
* Recuperación de la población
* Se están combinando tres efectos
* Se agrega un cuarto efecto: Reoxigenación
* Son las 4 R de la radioterapia
 
## Experimento 5
* Se implantan células tumorales en ratones
* Leucemia p388
* Se aplican dos dosis de radiación separadas y se registra la supervivencia relativa al experimento de dosis única
* No hay efecto de reorganización. El tiempo del ciclo celular es largo comparado con el experimento
* Leucemia: el efecto es mayor en los tumores que tienen menos tiempo de desarrollo (mejores condiciones, mejoran la reparación SLD)
 
## Experimento 6
* Se irradia piel en ratones con dos fracciones iguales
* Se grafica la dosis total necesaria para obtener una supervivencia de 1 célula / mm^2
 
## Factores involucrados en la reparación SLD
 
## Mecanismos de reparación del SLD
* Debe consistir en la reparación de las rupturas de cadena doble
* Rupturas de la segunda fracción no consiguen a rupturas de la primera fracción
* Ya han reparado
* Se reduce la probabilidad de generar uniones incorrectas
* Menos aberraciones letales
 
## Mecanismos de reparación del SLD
* Hay una componente lineal en la dosis a las DSB
* Producen malas uniones y aberraciones
* Es independiente del fraccionamiento
* La componente cuadrática en la dosis es la que se reduce con el fraccionamiento
 
 
## Calidad de la radiación
* La capacidad de reparar SLD depende de la calidad de la radiación
* Se evidencia en el hombro de la curva de supervivencia
* A menor capacidad de reparación de SLD el hombro es menos pronunciado
* Se manifiesta al comparar alta y baja LET
* Fig: Radiology. 1975;117:173–178
 
## Experimento 6
* Células V79, Hámster chino, cultivadas in vitro.
* Se usan rayos X de 210 kV y neutrones de 35 MeV (rápidos)
* Fotones: tratamiento en dos fracciones de 4 Gy vs una fracción de 8 Gy
* Neutrones: tratamiento en dos fracciones de 1,4 Gy vs una fracción de 2,8 Gy
* Se reporta el factor de recuperación vs tiempo entre fracciones
* Factor de recuperación es el cociente de la fracción de supervivencia fraccionada con respecto a la de dosis única
* La recuperación de neutrones es mucho más baja
 
## Efecto de tasa de dosis
* Tasa de dosis
* Es la rapidez con la cual de está entregando energía en el medio
* A mayor tasa de dosis, menor será el tiempo de exposición
* A menor tasa de dosis, mayor será el tiempo de exposición
 
## Efecto de tasa de dosis
* Al observar el efecto de la reparación del SLD se espera que a bajas tasas de dosis haya efectos importantes en la supervivencia a fotones
* La curva de supervivencia se hace más plana
* El límite estaría en la pendiente inicial de la curva de supervivencia
 
 
## Efecto inverso de tasa de dosis
* Ocurre en algunos cultivos
* Hay un punto de transición para el cual la pendiente de la curva de supervivencia aumenta al bajar la tasa de dosis
* Importante
* Tasas altas: las células se detienen en el ciclo celular
* Punto crítico de tasa de dosis: las células progresan hasta el punto de mayor radiosensibilidad, en G2, donde se detienen
* Tasas de dosis menores: las células progresan en el ciclo celular y se multiplican	
 
## Efecto inverso de tasa de dosis
* Células HeLa muestran efecto inverso de tasa de dosis
* A partir de 1,5 Gy/h, el efecto aumenta con la tasa de dosis
* En 0,428 Gy/h se alcanza el mayor efecto
* En 0,37 Gy/h el efecto se ha reducido de nuevo
 
## Efecto inverso de tasa de dosis
* Resumen
* Mayor tasa de dosis, se reduce el hombro
* Al bajar tasa de dosis se favorece la reparación del SLD y la curva se hace más plana
* En los rangos de tasa de dosis alta, las células se detienen en el ciclo
* En algunos casos hay un punto de transición de tasa de dosis, donde el efecto vuelve a aumentar aunque la tasa de dosis baje
* Células de detienen en punto radiosensible de G2
* Para tasas menores las células continúan el ciclo y se dividen
 
## Efecto de la apoptosis
* Células que manifiestan apoptosis como mecanismo de muerte celular, no muestran efectos importantes de tasa de dosis
* Recordar, la contribución de la apoptosis a la relación dosis efecto es lineal. No produce hombro
* Por ejemplo, apoptosis es mayor en HeLa vs Hámster. Se evidencia en la amplitud del hombro y del efecto de tasa de dosis 
 
## Efecto tasa de dosis in vivo
* Experimento 7
* Se aplican fotones de 0,6617 MeV a cuerpo completo de ratones a diferentes tasas de dosis
* Luego de 3 días se evalúa la supervivencia de las células en las criptas del yeyuno, contando la densidad de micro colonias que han formado
 
## Células de origen humano
* Gran variedad de tiempos de reparación
* Ensayos in vitro