# Clase 1: La célula: Composición química y estructura celular
## Reglas del juego
* El curso tiene 16 clases
* Datos
    * José Antonio López Rodríguez
    * jal.ccs@gmail.com
* Fechas 2024-01-15 al 2024-04-25
* Evaluación
    * Exámenes 
        * 2024-02-05 (25%)
        * 2024-02-26 (25%)
        * 2024-03-11 (25%)
        * 2024-03-25 (25%)
## Resumen
* El curso tiene 4 grandes partes
    * De la célula a la dinámica de los tejidos
    * Fundamentos del efecto de la radiación sobre la materia viva
    * Modificadores del efecto de la radiación
    * Efectos sobre el ser humano y su protección
### Componentes que conforman la célula
* Definición: Estructura autónoma e independiente, delimitada por una membrana que controla el transporte de sustancias del interior al exterior.
### Componentes
* Agua
* Sales minerales
* Componentes orgánicos
    * Proteinas
    * Carbohidratos
    * Ácidos Nucléicos
    * Lípidos
### Agua
* Es el componente más abundante (>70 %)
* Es medio para el metabolismo celular
* Es solvente universal (salvo lípidos)
    * Transporte de compuestos en interior celular
* Capacidad calórica alta
    * Protege de cambios bruscos de temperatura
### Sales minerales
* Sales de sodio y potasio
    * Potasio está más concentrado en interior de la célula
    * Sodio, más concentrado en el exterior
* Son fundamentales para la actividad celular
### Compuestos orgánicos
* Proteinas (15%)
    * Base estructural de las células
* Carbohidratos (1%)
    * Son fuente de enegía
* Ácidos nucléicos(1%)
    * ADN ARN
* Lípidos (2%)
    * Membrana celular
### Clasificación
* Eucariota
    * Núcleo diferenciado
    * Estructura de compartimientos
    * L~100 mm
    * Endosimbiosis (?)
* Procariota
    * Nucleoide
    * Bacteria y Archaea
    * L~10 mm
### Tamaño
* Limitado por balance entre transporte en membrana y metabolismo interno.
    * Transporte: T ~ masa/(área*tiempo)
    * Metabolismo: C ~ masa/(volumen*tiempo)
* Sea V el volumen de la célula, A el area superficial y L una longitud característica. $V=AL$
$$VC<AT$$
$$L<\frac{T}{C}$$
* Células de metabolismo acelerado serán más pequeñas.
### Trucos para crecer
* Límite prolato
    * Se disminuye el cociente volumen-superficie alargando la estructura
    * Neuronas
* Límite oblato
    * Se disminuye el cociente volumen-superficie aplanando la estructura
    * Estructuras internas
### Función del núcleo
* El tamaño de la célula también está limitado por la velocidad a la que el núcleo provee los ingredientes del metabolismo celular
    * Células grandes tendrán muchos núcleos
### Estructuras (eucariota)
* Membrana celular
* Núcleo
* Citoplasma
    * Ribosomas
    * Citoesqueleto
* Endomembranas
    * Vacuolas y vesículas
    * Retículo endoplasmático
    * Complejo de Golgi
    * Lisosomas
    * Mitocondrias
### Membrana celular
* Separa la célula del exterior
    * Doble capa lipídica (fosfolípidos, triglicéridos)
    * Colesterol (rigidez)
    * Ácidos grasos saturados=
    * +hidrofóbica=
    * +cohesión=
    * -fluidez de membrana
* Regula el transporte de sustancias
    * Presencia de:
        * Canales
        * Proteinas
        * Carbohidratos
        * Glucolípidos
* Las membranas favorecen muchos procesos bioquímicos. Hay otras estructuras de este tipo además de la capa exterior (en el interior de la célula)
### Membranas
* Las membranas favorecen muchos procesos bioquímicos.
    * Sostén para las moléculas participantes.
    * Pueden aislar microclimas dentro de la célula (digestión)
    * Hay multitud de endomembranas en la célula eucariota
### Núcleo
* Delimitado por una membrana doble
    * Intersticio de 20-40 nm
    * Posese poros nucleares para intercambio de sustancias
* Almacena la información hereditaria
    * Núcleo espermatozoide lleva información del padre
### Núcleo
* Se producen moleculas complejas requeridas constantemente por la célula
    * Una célula sin núcleo muere al poco tiempo
### Ribosomas
* Estructuras formadas por ARN y proteinas
* Lugar donde se sintetizan las proteinas
* Se distribuyen en el citoplasma o se agrupan sobre retículos endoplasmáticos
### Citoesqueleto
* Es responsable de las funciones mecánicas de la célula
    * Microtúbulos
        * Diametro ~22 nm
        * Fáciles de crear y remover
        * Funciones de transporte interno de sustancias
### Citoesqueleto
* Microfilamentos
    * Diametro ~ 6 nm
    * Fáciles de crear y remover
    * Funciones de movimiento celular y división
* Filamentos intermedios
    * Diámetro  7-11 nm
    * Difíciles de remover
    * Forma de la célula
### Vesículas
* Formadas por una membrana que aísla una porción de la célula
    * Almacenamiento temporal de sustancias
    * Transporte y procesos metabólicos
### Retículo endoplasmático
* Continuo con la pared nuclear
    * Rugoso: contiene ribosomas
        * Síntesis de proteinas
        * Grandes cantidades en células que exportan proteinas
    * Liso
        * Síntesis y metabolismo de lípidos
### Complejo de Golgi
* Estructura de sacos aplanados en secuencia
    * Cisternas y membranas
* Recolección, procesamiento final y distribución de sustancias
    * Intercambian los productos mediante vesículas (las crean y las absorben)
* Producen vesículas de secreción, nuevas secciones de membrana, lisosomas, etc
### Lisosomas
* Vesículas especializadas, contienen encimas hidrolíticas
    * Degradan macromoléculas
    * Deben estar aisladas del resto de la célula
    * Participan en procesos de digestión en la fagocitosis, en el sistema inmune.
### Mitocondrias
* Procesan moléculas orgánicas
    * Membranas plegadas
    * Generan energía que almacenan en ATP
    * +energía <-> +mitocondrias
    * Son el motor de la célula
    * Poseen ribosomas y ADN propio. Son sospechosas de endosimbiosis