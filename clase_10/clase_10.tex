\documentclass[aspectratio=169]{beamer}
\usetheme{Madrid}
\usecolortheme{default}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

%strikeout
\usepackage{soul}

%\usepackage[sfdefault]{roboto}
\usefonttheme[onlymath]{serif}
\usepackage{tikz}
\usepackage{tikzsymbols}
\usetikzlibrary{hobby}
\usepackage{pgfplots}


%Colores paleta manual de marca
\definecolor{LCblueInst}{RGB}{50, 50, 123}
\definecolor{LCredInst}{RGB}{237, 28, 36}
\definecolor{LCblueSec1}{RGB}{21, 9, 88}
\definecolor{LCblueSec2}{RGB}{191, 193, 215}
\definecolor{LCblueSec3}{RGB}{247, 248, 249}

%Colores aproximados a los usados en el logo
\definecolor{logoyellow}{RGB}{245, 226, 0}
\definecolor{logobrown}{RGB}{117, 80, 45}
\definecolor{logobrownD}{RGB}{86, 42, 22}

\usepackage{pgf,tikz}
\usepackage{pgfplots}
\usetikzlibrary{shapes,arrows,backgrounds}

\usepackage{natbib}   
\bibliographystyle{plainnat}


\title{Radiobiología}
\subtitle{Clase 10: Radiosensibilidad y el ciclo celular}


\author{José Antonio López Rodríguez} % Your name
\institute[UCV] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Universidad Central de Venezuela \\ % Your institution for the title page
\textit{jose.lopez@ucv.ve} % Your email address
}
\date{01 de marzo de 2024} % Date, can be changed to a custom date

\begin{document}

\frame{\titlepage}

\section{Contenido}
\begin{frame}{Contenido}
\begin{itemize}
    \item Sincronización de cultivos celulares
    \item Cosecha mitótica
    \item Uso de hidroxiurea
    \item Determinación de duraciones del ciclo celular y variación de la resistencia a la radiación
    \item Punto de control G1
    \item Resistencia y ciclo celular en para tejido in-vivo
\end{itemize}
\end{frame}

\section{Preliminares}
\begin{frame}{Preliminares}
\begin{itemize}
    \item Hay evidencia de que la radiosensibilidad cambia a lo largo del ciclo celular
    \item Recordar: la radiosensibilidad de las células de mamífero en etapa mitótica es similar
    \item Necesitamos poblaciones sincronizadas
    \item Debemos conocer con precisión la duración de las fases del ciclo
    \item Es conveniente estudiar qué ocurre en otras etapas del ciclo
\end{itemize}
\end{frame}

\section{Técnicas de sincronización}
\begin{frame}{Técnicas de sincronización}
\begin{itemize}
    \item Cosecha mitótica
    \item Uso de hidroxiurea
\end{itemize}
\end{frame}

\section{Cosecha mitótica}
\begin{frame}{Cosecha mitótica}
\begin{itemize}
    \item Se aplica en células cultivadas en una capa sobre el medio de cultivo
    \item Al comenzar la mitosis la célula cambia su forma y pierde parcialmente su unión con el medio
    \item Las células en pre-mitosis se desprenden mediante un movimiento suave. Quedan flotando en el medio.
    \item Se retiran y se siembran en una nueva placa. Están sincronizadas
\end{itemize}
\end{frame}

\section{Hidroxiurea}
\begin{frame}{Hidroxiurea}
\begin{columns}[c] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment

\column{.45\textwidth}
\begin{itemize}
    \item Al agregar hidroxiurea, todas las células en fase S mueren
    \item La droga impone un bloqueo que impide a las células entrar en fase S
    \item Las células sobrevivientes progresan en el ciclo celular y se detienen al final de G1
    \item Si espera un tiempo mayor al ciclo celular y se retira la droga se tiene una población sincronizada
\end{itemize}
\column{.45\textwidth}

\centering
		\begin{figure}
		\includegraphics[scale=0.5]{fig/hidroxi.png}
		\caption{Efecto de la hidroxiurea}
		\end{figure}

\end{columns}
\end{frame}

\begin{frame}
\frametitle{Poblaciones de prueba}

\begin{columns}[c] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment

\column{.45\textwidth}
\begin{itemize}
\item Células de Hamster chino y HeLa
\item Duraciones del ciclo celular diferentes
\item Diferencia principal en la fase G1
\item Patrón común
\end{itemize}
\column{.45\textwidth}

\centering
		\begin{figure}
		\includegraphics[scale=0.5]{fig/hamsterVsHela.png}
		\caption{Efecto de la hidroxiurea}
		\end{figure}

\end{columns}
\end{frame}

\section{Experimentos}

\subsection{Experimento 1}

\begin{frame}
\frametitle{Experimento 1}
\begin{columns}[c] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment

\column{.45\textwidth}
\begin{itemize}
\item Células de hámster cosechadas en mitosis
\item Se aplica una dosis única de 6,6 Gy en diferentes momentos a lo largo del ciclo
\item Se cuenta la fracción de células sobrevivientes
\item Mínimo de resistencia en inicio de G1
\item Resistencia aumenta durante el progreso de S
\item Resistencia desciende nuevamente en el transcurso de G2 y M
\end{itemize}
\column{.45\textwidth}

\centering
		\begin{figure}
		\includegraphics[scale=0.5]{fig/HamsterCicloResis.png}
		\caption{Efecto de la hidroxiurea}
		\end{figure}

\end{columns}
\end{frame}

\subsection{Experimento 2}

\begin{frame}
\frametitle{Experimento 2: Curvas de supervivencia}

\begin{columns}[c] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment

\column{.45\textwidth}

\begin{itemize}
\item Curvas de supervivencia en distintas fases
\item M y G2 no hay hombro. Pendiente pronunciada
\item Final de S (LS) hay un hombro extenso
\item Las otras fases, G1 y S temprana (ES) tienen hombros que están entre los dos comportamientos límite
\item Línea punteada es calculo de supervivencia para células mitóticas en condiciones de hipoxia (factor modificador)
\end{itemize}

\column{.45\textwidth}

\centering
		\begin{figure}
		\includegraphics[scale=0.35]{fig/HamsterSenCadaFase.png}
		\caption{Efecto de la hidroxiurea}
		\end{figure}

\end{columns}

\end{frame}

\subsection{Experimento 3}

\begin{frame}
\frametitle{Experimento 3: Células HeLa sincronizadas}

\begin{columns}[c] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment

\column{.45\textwidth}

\begin{itemize}
\item Poblaciones HeLa sincronizadas por cosecha mitótica
\item Se aplica una dosis de 3 Gy en varios momentos luego de la mitosis
\item Igual método que experimento 1
\item El patrón tiene algunas similitudes
\item Hay un pico de resistencia en G1
\item No es visible con las células de hámster (fase G1 muy corta)
\end{itemize}

\column{.45\textwidth}

\centering
		\begin{figure}
		\includegraphics[scale=0.35]{fig/HeLaCicloResis.png}
		\caption{Efecto de la hidroxiurea}
		\end{figure}

\end{columns}
\end{frame}

\begin{frame}
\frametitle{Comparación HeLa – Hamster chino}

\begin{columns}[c] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment

\column{.45\textwidth}

\begin{itemize}
\item Pico de resistencia es evidente
\item Si se repite estudio en células de ratón con períodos G1 más largos, hay aumento de resistencia
\end{itemize}

\column{.45\textwidth}

\centering
		\begin{figure}
		\includegraphics[scale=0.35]{fig/compHamsterHelaSensib.png}
		\caption{Efecto de la hidroxiurea}
		\end{figure}

\end{columns}
\end{frame}

\subsection{Experimento 4}

\begin{frame}
\frametitle{Experimento 4: Resolución de la fase G2}
\begin{itemize}
\item El método descrito tiene mala resolución de la fase G2
\item Es la más lejana a la mitosis
\item Hay dispersión en los tiempos de duración de los periodos celulares de cada individuo
\item Se plantea la cosecha inversa
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Experimento 4: Seguimiento de células}
\begin{itemize}
\item Población celular asíncrona es irradiada
\item Se cosechan las células que van llegando a la mitosis
\item El tiempo de espera permite establecer la fase del ciclo que ocupaban al momento de la irradiación
\item Se verifica que las células transitan de un período de alta resistencia al final de S hasta un período sensible al iniciar la mitosis
\item En G2 se identifica el punto de transición para rayos X, que marca el cambio de sensibilidad.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Experimento 4: Resultados}
\begin{itemize}
\item Sensibilidad crece antes, durante y después de la mitosis
\item Resistencia es mayor al final de la etapa de síntesis
\item Si G1 es largo se aprecia un aumento de la resistencia, seguido de una disminución hacia el final de la fase
\item G2 puede llegar a ser tan sensible como la mitosis
\end{itemize}
\end{frame}

\subsection{Punto de control G2}

\begin{frame}
\frametitle{Punto de control G2}
\begin{itemize}
\item Se observa que las células irradiadas sufren un bloqueo en el punto de transición de sensibilidad en G2
\item Se sospecha que la función de este checkpoint es permitir la reparación del DNA dañado
\item Si la dosis se aplica después del punto de control, no hay reparación posible y la célula entra en mitosis con grandes probabilidades de fallar
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Punto de control G2}

\begin{columns}[c] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment

\column{.45\textwidth}

\begin{itemize}
\item Este checkpoint está regulado genéticamente
\item Si la célula ha perdido el gen relacionado por una mutación la sensibilidad puede aumentar en factores grandes (10 a 100)
\end{itemize}

\column{.45\textwidth}

\centering
		\begin{figure}
		\includegraphics[scale=0.5]{fig/G2CheckPoint.png}
		\caption{Efecto de la hidroxiurea}
		\end{figure}

\end{columns}
\end{frame}

\subsection{Resistencia in vivo}

\begin{frame}
\frametitle{Experimento 5: Resistencia in vivo}


\begin{itemize}
\item Se aplica hidroxiurea inyectada en yeyuno de ratones cada hora por 5 horas
\item Células en S mueren y las otras quedan represadas en la interfaz G1/S
\item Se se da una dosis de 11 Gy de fotones a varios tiempos después de la sincronización
\item Para identificar la fase del cultivo celular se inyecta un marcador del DNA y se revisa su asimilación en nuevo DNA.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Resistencia y el ciclo celular para tejido in-vivo}
\begin{itemize}
\item Se cuenta el número de células por área transversal de yeyuno
\item Se usa el marcador para establecer con precisión eventos concretos en el ciclo celular
\item La sensibilidad muestra los mismos patrones que se obtuvieron con los estudios in vitro
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Variación de la resistencia de acuerdo al ciclo celular para radiación de alta LET}

\begin{columns}[c] % The "c" option specifies centered vertical alignment while the "t" option is used for top vertical alignment

\column{.45\textwidth}
\begin{itemize}
\item La curva de sensibilidad para neutrones es similar a la de fotones
\item Lo que las diferencia es que toda la curva de supervivencia de neutrones muestra mayor sensibilidad
\item Los efectos con neutrones se obtienen para 16 MeV y 50 MeV, mostrando el efecto de la captura de neutrones
\end{itemize}

\column{.45\textwidth}

\centering
		\begin{figure}
		\includegraphics[scale=0.5]{fig/invivo.png}
		\caption{Efecto de la hidroxiurea}
		\end{figure}

\end{columns}
\end{frame}

\begin{frame}
\frametitle{Reparación y ciclo celular}
\begin{itemize}
\item Variación de sensibilidad durante el ciclo celular tiene consecuencias en radioterapia
\item Efectos distintos para células en distintas fases
\item Un tratamiento de dosis única tenderá a sincronizar parcialmente la población, seleccionando las células en las zonas resistentes del ciclo
\item La evolución de las células en el ciclo puede mover células a las zonas sensibles del ciclo en la próxima fracción del ciclo celular
\end{itemize}
\end{frame}

\end{document}