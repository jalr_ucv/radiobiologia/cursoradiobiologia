# Clase 10: Radiosensibilidad y el ciclo celular

## Contenido
* Sincronización de cultivos celulares
* Cosecha mitótica
* Uso de hidroxiurea
* Determinación de duraciones del ciclo celular y variación de la resistencia a la radiación
* Punto de control G1
* Resistencia y ciclo celular en para tejido in-vivo

## Preliminares
* Hay evidencia de que la radiosensibilidad cambia a lo largo del ciclo celular
* Recordar: la radiosensibilidad de las células de mamífero en etapa mitótica es similar
* Necesitamos poblaciones sincronizadas
* Debemos conocer con precisión la duración de las fases del ciclo
* Es conveniente estudiar qué ocurre en otras etapas del ciclo

## Técnicas de sincronización
* Cosecha mitótica
* Uso de hidroxiurea

## Cosecha mitótica
* Se aplica en células cultivadas en una capa sobre el medio de cultivo
* Al comenzar la mitosis la célula cambia su forma y pierde parcialmente su unión con el medio
* Las células en pre-mitosis se desprenden mediante un movimiento suave. Quedan flotando en el medio.
* Se retiran y se siembran en una nueva placa. Están sincronizadas

## Hidroxiurea
* Al agregar hidroxiurea, todas las células en fase S mueren
* La droga impone un bloqueo que impide a las células entrar en fase S
* Las células sobrevivientes progresan en el ciclo celular y se detienen al final de G1
* Si espera un tiempo mayor al ciclo celular y se retira la droga se tiene una población sincronizada

## Poblaciones de prueba
* Se muestran las duraciones del ciclo de células provenientes de Hamster chino y HeLa
* Las duraciones de los dos ciclos son totalmente distintas
* La diferencia está principalmente en la fase G1
* Es un patrón común

## Experimento 1
* Células de hámster cosechadas en mitosis
* Se aplica una dosis única de 6,6 Gy en diferentes momentos a lo largo del ciclo
* Se cuenta la fracción de células sobrevivientes
* Mínimo de resistencia en inicio de G1
* Resistencia aumenta durante el progreso de S
* Resistencia desciende nuevamente en el transcurso de G2 y M

## Experimento 2
* Curvas de supervivencia en distintas fases
* M y G2 no hay hombro. Pendiente pronunciada
* Final de S (LS) hay un hombro extenso
* Las otras fases, G1 y S temprana (ES) tienen hombros que están entre los dos comportamiento límite
* Línea punteada es calculo de supervivencia para células mitóticas en condiciones de hipoxia (factor modificador)

## Experimento 3
* Poblaciones HeLa sincronizadas por cosecha mitótica
* Se aplica una dosis de 3 Gy en varios momentos luego de la mitosis
* Igual método que experimento 1
* El patrón tiene algunas similitudes
* Hay un pico de resistencia en G1
* No es visible con las células de hámster (fase G1 muy corta)

## Comparación HeLa – Hamster chino
* Pico de resistencia es evidente
* Si se repite estudio en células de ratón con períodos G1 más largos, hay aumento de resistencia

## Experimento 4
* El método descrito tiene mala resolución de la fase G2
* Es la más lejana a la mitosis
* Hay dispersión en los tiempos de duración de los periodos celulares de cada individuo
* Se plantea la cosecha inversa

## Experimento 4
* Población celular asíncrona es irradiada
* Se cosechan las células que van llegando a la mitosis
* El tiempo de espera permite establecer la fase del ciclo que ocupaban al momento de la irradiación
* Se verifica que las células transitan de un período de alta resistencia al final de S hasta un período sensible al iniciar la mitosis
* En G2 se identifica el punto de transición para rayos X, que marca el cambio de sensibilidad.

## Resumen
* Sensibilidad crece antes, durante y después de la mitosis
* Resistencia es mayor al final de la etapa de síntesis
* Si G1 es largo se aprecia un aumento de la resistencia, seguido de una disminución hacia el final de la fase
* G2 puede llegar a ser tan sensible como la mitosis

## Punto de control G2
* Se observa que las células irradiadas sufren un bloqueo en el punto de transición de sensibilidad en G2
* Se sospecha que la función de este checkpoint es permitir la reparación del DNA dañado
* Si la dosis se aplica después del punto de control, no hay reparación posible y la célula entra en mitosis con grandes probabilidades de fallar


## Punto de control G2
* Este checkpoint está regulado genéticamente
* Si la célula ha perdido el gen relacionado por una mutación la sensibilidad puede aumentar en factores grandes (10 a 100)

## Resistencia y el ciclo celular in vivo
* Se aplica hidroxiurea inyectada en yeyuno de ratones cada hora por 5 horas
* Células en S mueren y las otras quedan represadas en la interfaz G1/S
* Se se da una dosis de 11 Gy de fotones a varios tiempos después de la sincronización
* Para identificar la fase del cultivo celular se inyecta un marcador del DNA y se revisa su asimilación en nuevo DNA.

## Resistencia y el ciclo celular para tejido in-vivo
* Se cuenta el número de células por área transversal de yeyuno
* Se usa el marcador para establecer con precisión eventos concretos en el ciclo celular
* La sensibilidad muestra los mismos patrones que se obtuvieron con los estudios in vitro

## Variación de la resistencia de acuerdo al ciclo celular para radiación de alta LET
* La curva de sensibilidad para neutrones es similar a la de fotones
* Lo que las diferencia es que toda la curva de supervivencia de neutrones muestra mayor sensibilidad
* Los efectos con neutrones se obtienen para 16 MeV y 50 MeV, mostrando el efecto de la captura de neutrones


## Reparación y ciclo celular
* Variación de sensibilidad durante el ciclo celular tiene consecuencias en radioterapia
* Efectos distintos para células en distintas fases
* Un tratamiento de dosis única tenderá a suavizar parcialmente la población, seleccionando las células en las zonas resistentes del ciclo
* La evolución de las células en el ciclo puede mover células a las zonas sensibles del ciclo en la próxima fracción del ciclo celular
